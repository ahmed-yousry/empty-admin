<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fname');
            $table->string('phone');



            $table->string('seg');

            $table->string('email');
            $table->string('password');

            $table->string('remember_token');

            $table->integer('class_room_id')->unsigned();
            $table->foreign('class_room_id')->references('id')->on('classes')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->integer('mom_id')->unsigned()->nullable();
            $table->foreign('mom_id')->references('id')->on('momes')
                 ->onUpdate('cascade')
                 ->onDelete('cascade');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
