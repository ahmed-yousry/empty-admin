<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
             Schema::create('Notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('students')
                ->onUpdate('cascade')
                ->onDelete('cascade');

     
               $table->string('title');
                
                $table->string('content');
                $table->string('seen');
              
                $table->string('seg');
            $table->timestamps();
        }); 
    
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('Notifications');
    }
}
