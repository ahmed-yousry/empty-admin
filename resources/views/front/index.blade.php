@extends('front.layouts.master')
@section('title')
    الرئيسية
@endsection
@section('css')
    <link  rel="stylesheet" href="{{asset('assets/front/css/homepage.css')}}">@endsection
@section('content')

    <section class="main">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                @foreach($sliders as $slider)
                    <li data-target="#carouselExampleIndicators" data-slide-to="{{$loop->index}}" @if($loop->first) class="active" @endif ></li>
                @endforeach

                {{--<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>--}}
                {{--<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>--}}
                {{--<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>--}}
                {{--<li data-target="#carouselExampleIndicators" data-slide-to="3"></li>--}}

            </ol>
            <div class="carousel-inner">
                @foreach($sliders as $slider)
                    <div class="carousel-item @if($loop->first) active @endif">
                        <img class="d-block w-100" src="{{asset('public/uploads/slider/').'/'.$slider->photo}}" alt="First slide">
                    </div>
                @endforeach
                {{--<div class="carousel-item">--}}
                    {{--<img class="d-block w-100" src="{{asset('assets/front/images/slider-2.jpg')}}" alt="Second slide">--}}
                {{--</div>--}}
                {{--<div class="carousel-item">--}}
                    {{--<img class="d-block w-100" src="{{asset('assets/front/images/slider-1.jpg')}}" alt="Third slide">--}}
                {{--</div>--}}
            </div>
        </div>
        <div class="overlay">
            <img src="{{asset('assets/front/images/logo2.png')}}" class="img-fluid" alt="estasmer">
        </div>
    </section>
    <section class="search-now text-center">
        <div class="container">
            <h2 class="title"> ابحث الآن عن وحدتك</h2>
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12 form-group">
                    <select name="type" title="Property Types" class="form-control">
                        <option value="فيلات">فيلات</option>
                        <option value="فيلات">فيلات</option>
                        <option value="فيلات">فيلات</option>
                        <option value="" selected=""> شقق </option>
                    </select>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 form-group">
                    <select name="city" title="Property Types" class="form-control">
                        <option value="الاسكندرية">الاسكندرية</option>
                        <option value="الاسكندرية">الاسكندرية</option>
                        <option value="الاسكندرية">الاسكندرية</option>
                        <option value="" selected=""> الاسكندرية </option>
                    </select>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 form-group">
                    <select name="District" title="Property Types" class="form-control">
                        <option value="الحى">الحى</option>
                        <option value="الحى">الحى</option>
                        <option value="الحى">الحى</option>
                        <option value="" selected=""> الحى </option>
                    </select>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 form-group">
                    <select name="for-sale"  title="Min Price" class="form-control">
                        <option value="للبيع">للبيع</option>
                        <option value="للبيع">للبيع</option>
                        <option value="للبيع">للبيع</option>
                        <option value="" selected=""> للبيع </option>
                    </select>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 form-group">
                    <select name="min-price" class="form-control">
                        <option value="">أقل سعر</option>
                        <option value="أقل سعر">أقل سعر</option>
                        <option value="أقل سعر">أقل سعر</option>
                    </select>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 form-group">
                    <select name="min-price" class="form-control">
                        <option value="">أعلى سعر</option>
                        <option value="أعلى سعر">أعلى سعر</option>
                        <option value="أعلى سعر">أعلى سعر</option>
                    </select>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 form-group">
                    <select name="min-area" class="form-control">
                        <option value=""> اقل مساحة</option>
                        <option value=""> اقل مساحة</option>
                        <option value=""> اقل مساحة</option>
                    </select>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 form-group">
                    <select name="max-area" class="form-control">
                        <option value="">  أكبر مساحة </option>
                        <option value="">  أكبر مساحة </option>
                        <option value="">  أكبر مساحة </option>
                    </select>
                </div>
                <div class="col align-self-center">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="customCheck" name="save-search">
                        <label class="custom-control-label" for="customCheck"> حفظ البحث </label>
                    </div>
                    <button type="button" class="btn btn-primary"> بحث </button>
                </div>
                <!-- <div class="col-md-4 col-sm-6 col-xs-12 form-group submit-search-form pull-right">
                </div> -->
            </div>
        </div>
    </section>
    <section class="featured-appartments text-center">
        <div class="container">
            <h2 class="title">وحدات مميزة</h2>
            <div class="row">
                <div class="col-md-4">
                    <div class="price wow animate fadeIn animated" data-wow-delay=".2s"  data-wow-offset="200">
                        <img src="{{asset('assets/front/images/appartment.jpg')}}"alt="estasmer">
                        <div class="price-text">
                            <p>لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور   </p>
                            <ul class="list-unstyled">
                                <li class="red-txt">400,000 جنيه</li>
                                <li>المساحة :135 م </li>
                                <li>الحى: جليم </li>
                            </ul>
                            <a href=""class="btn btn-primary">اطلب الان</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="price wow animate fadeIn animated" data-wow-delay=".4s"  data-wow-offset="200">
                        <img src="{{asset('assets/front/images/appartment.jpg')}}"alt="estasmer">
                        <div class="price-text">
                            <p>لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور   </p>
                            <ul class="list-unstyled">
                                <li class="red-txt">400,000 جنيه</li>
                                <li>المساحة :135 م </li>
                                <li>الحى: جليم </li>
                            </ul>
                            <a href=""class="btn btn-primary">اطلب الان</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="price wow animate fadeIn animated" data-wow-delay=".6s"  data-wow-offset="200">
                        <img src="{{asset('assets/front/images/appartment.jpg')}}"alt="estasmer">
                        <div class="price-text">
                            <p>لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور   </p>
                            <ul class="list-unstyled">
                                <li class="red-txt">400,000 جنيه</li>
                                <li>المساحة :135 م </li>
                                <li>الحى: جليم </li>
                            </ul>
                            <a href=""class="btn btn-primary">اطلب الان</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="price wow animate fadeIn animated" data-wow-delay=".8s"  data-wow-offset="200">
                        <img src="{{asset('assets/front/images/appartment.jpg')}}"alt="estasmer">
                        <div class="price-text">
                            <p>لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور   </p>
                            <ul class="list-unstyled">
                                <li class="red-txt">400,000 جنيه</li>
                                <li>المساحة :135 م </li>
                                <li>الحى: جليم </li>
                            </ul>
                            <a href=""class="btn btn-primary">اطلب الان</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="price wow animate fadeIn animated" data-wow-delay="1s"  data-wow-offset="200">
                        <img src="{{asset('assets/front/images/appartment.jpg')}}"alt="estasmer">
                        <div class="price-text">
                            <p>لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور   </p>
                            <ul class="list-unstyled">
                                <li class="red-txt">400,000 جنيه</li>
                                <li>المساحة :135 م </li>
                                <li>الحى: جليم </li>
                            </ul>
                            <a href=""class="btn btn-primary">اطلب الان</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="price wow animate fadeIn animated" data-wow-delay="1.2s"  data-wow-offset="200">
                        <img src="{{asset('assets/front/images/appartment.jpg')}}"alt="estasmer">
                        <div class="price-text">
                            <p>لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور   </p>
                            <ul class="list-unstyled">
                                <li class="red-txt">400,000 جنيه</li>
                                <li>المساحة :135 م </li>
                                <li>الحى: جليم </li>
                            </ul>
                            <a href=""class="btn btn-primary">اطلب الان</a>
                        </div>
                    </div>
                </div>
                <div class="paging text-center">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item active"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>

    <section class="advertise">
        <div class="container">
            <h2 class="title">مساحة اعلانية</h2>
        </div>
    </section>
@endsection