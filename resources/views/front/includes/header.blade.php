<div class="header">

    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-7 col-sm-6">
                    <p class="phone-number"><i class="fas fa-phone"></i><span>{{\Facades\App\Helper\IceHelper::getSetting('Phone')}}</span></p>
                </div>
                <div class="col-5 col-sm-6">
                    <ul class="list-unstyled">
                        <li><a href="{{\Facades\App\Helper\IceHelper::getSetting('Facebook')}}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="{{\Facades\App\Helper\IceHelper::getSetting('Google_Plus')}}"target="_blank"> <i class="fab fa-google-plus-g"></i></a></li>
                        <li><a href="{{\Facades\App\Helper\IceHelper::getSetting('LinkedIn')}}"target="_blank">   <i class="fab fa-linkedin-in">    </i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <header id="sticker">
        <div class="container">
            <div class="row">
                <div class="col-3 col-lg-1  col-md-4 col-sm-4 order-3 order-lg-1">
                    <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('public/uploads/setting/photo/').'/'.\Facades\App\Helper\IceHelper::getSetting('Logo') }}"></a>
                </div>
                <div class="col-2 col-lg-8 col-md-2 col-sm-2 order-1  order-lg-2">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse">
                            <ul class="navbar-nav  mt-2 mt-lg-0">
                                <li class="nav-item active">
                                    <a class="nav-link" href="{{url('/')}}">بحث  <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('order')}}">طلبك</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="details.html"> مساعدة</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"> مدونة</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        اخرى
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="policy.html">الشروط والاحكام</a>
                                        <a class="dropdown-item" href="advices.html"> نصائح لمستخدمى الموقع</a>
                                        <a class="dropdown-item" href="packages.html">  الباقات</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <div class="col-7 col-lg-3 col-md-6 col-sm-6 order-2 order-lg-3">
                    <div class="left-icons">
                        <a class="login" href=""  data-toggle="modal" data-target="#loginModal"><span>Alaa</span><i class="far fa-user"></i></a>
                        <div class="dropdown">
                            <!-- <button type="button" class="btn  dropdown-toggle" data-toggle="dropdown">
                                اسم المستخدم
                            </button> -->
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="settings.html">الاعدادات</a>
                                <a class="dropdown-item" href="myorders.html">الطلبات </a>
                            </div>
                        </div>
                        <a href="new-add.html" class="btn btn-primary" ><span class="visible-lg"><i class="fas fa-plus"></i>اضف اعلان جديد</span><span class="visible-xs"><i class="fas fa-plus"></i> اعلان </span></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="collapse navbar-collapse"id="navbar" >
            <ul class="navbar-nav  mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="#">بحث  <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="order.html">طلبك</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="details.html"> مساعدة</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"> مدونة</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        اخرى
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="policy.html">الشروط والاحكام</a>
                        <a class="dropdown-item" href="advices.html"> نصائح لمستخدمى الموقع</a>
                        <a class="dropdown-item" href="packages.html">  الباقات</a>
                    </div>
                </li>
            </ul>
        </div>
    </header>
    <!-- loginModal -->
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <ul class="nav nav-tabs">
                        <li class="nav-item active"><a class="nav-link" data-toggle="tab" href="#login">تسجيل دخول</a></li>
                        <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#register">اشترك</a></li>
                    </ul>
                </div>
                <div class="modal-body">
                    <div class="tab-content">
                        <div id="login" class="tab-pane active">
                            <form>
                                <div class="form-group user">
                                    <input type="text" class="form-control" id="username" placeholder="اسم المستخدم">
                                </div>
                                <div class="form-group password">
                                    <input type="password" class="form-control" id="pwd" placeholder="كلمة المرور">
                                </div>
                                <div class="clear">
                                    <div class="form-group form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox"> تذكرنى
                                        </label>
                                    </div>
                                    <a href="#" class="forget">نسيت كلمة المرور</a>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary btn-block">تسجيل الدخول</button>
                                </div>
                            </form>
                        </div>
                        <div id="register" class="tab-pane fade">
                            <form>
                                <div class="form-group user">
                                    <input type="text" class="form-control" placeholder="اسم المستخدم">
                                </div>
                                <div class="form-group email">
                                    <input type="email" class="form-control" placeholder="البريد الالكترونى">
                                </div>
                                <div class="form-group password">
                                    <input type="password" class="form-control"  placeholder="كلمة المرور">
                                </div>
                                <div class="form-group password">
                                    <input type="password" class="form-control"  placeholder="تأكيد كلمة المرور">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary btn-block">تسجيل الدخول</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End loginModal -->

</div>