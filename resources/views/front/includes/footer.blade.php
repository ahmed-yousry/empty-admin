<div class="footer">


    <footer>
        <div class="top">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="text-center">
                            <img src="{{asset('assets/front/images/logo-small2.png')}}"alt="sophia-kitchen">
                            <p>{{\Facades\App\Helper\IceHelper::getSetting('Description')}}
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="contact">
                            <h4>تواصل معنا</h4>
                            <ul class="list-unstyled">
                                <li><i class="fas fa-map-marker-alt"></i></i>
                                    <span>{{\Facades\App\Helper\IceHelper::getSetting('Address')}}</span>
                                </li>
                                <li><i class="fa fa-phone"></i>{{\Facades\App\Helper\IceHelper::getSetting('Phone')}}</li>
                                <li><i class="far fa-envelope"></i>{{\Facades\App\Helper\IceHelper::getSetting('Email')}}</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h4>اشتراك</h4>
                        <form>
                            <div class="form-group">
                                <label for="email"></label>
                                <input type="email" class="form-control" id="email"placeholder="البريد الالكترونى">
                            </div>
                            <button type="submit"class="btn btn-primary">موافق</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyrights">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p> جميع الحقوق محفوظة © 2018  <a href="http://www.backdel.com/" target="_blank">backdel</a></p>
                    </div>
                </div>
            </div>
        </div>

    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="js/jquery-3.3.1.min.js"></script> -->

    <!-- <script>
       new WOW().init();
      $("#sticker").sticky({topSpacing:0});
    </script>
  </body>
</html> -->

</div>
