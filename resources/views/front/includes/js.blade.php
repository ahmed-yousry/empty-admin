<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{asset('assets/front/js/popper.min.js')}}"></script>
<script src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/front/js/wow.min.js')}}"></script>
<script src="{{asset('assets/front/js/jquery.sticky.js')}}"></script>
<script>

    $(document).ready(function(){
        $(".loading").fadeOut(1000);
        $(".header").load("header.html", function() {
            $("#sticker").sticky({topSpacing:0});
            new WOW().init();
        });
        $(".footer").load("footer.html");
    });


</script>