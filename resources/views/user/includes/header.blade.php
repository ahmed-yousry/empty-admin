<header class="main-header">
    <!-- Logo -->
    <a href="{{url(LaravelLocalization::setLocale().'/home')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>I</b>CE</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Alex</b> System</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

 
        
 
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
<!--  notifcation    -->
<li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <i class="fa fa-bell-o"></i>
          
              
          
              <span class="label label-warning"> {{app('count')}} </span>
           
            </a>
          
            <ul class="dropdown-menu">
              <li class="header">You have {{app('count')}} new notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                @foreach(app('Notification')  as $Notification)
                  <li>

                 
                    <a href=" {{ url('home/Notification/'. $Notification->id)}}">
                      <i class="fa fa-users text-aqua"></i> {{$Notification->title}}
                    </a>
                  </li>
                  @endforeach
            
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          
          </li>

          
<!--  notifcation    -->


 

<!-- message  -->
            <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">{{app('count_group_messages')}} </span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have {{app('count_group_messages')}} messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                @foreach(app('group_messages')  as $datas)
                @foreach($datas  as $data)
            
                  <li><!-- start message -->
                  
                    <a href="  {{url('home/messages/'.$data->id)}}  ">
                      <div class="pull-left">
                        <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
                      </div>
                      <h4>
                      {{$data->title}}  
                        <small><i class="fa fa-clock-o"></i> {{$data->created_at}}</small>
                      </h4>
                      <p> {{$data->content}}</p>
                    </a>
                  </li>
                  @endforeach
                  @endforeach
                </ul>



              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
<!-- message  -->


                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ asset('assets/dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
                        <span class="hidden-xs">            {{\Illuminate\Support\Facades\Auth::guard('web')->user()->name}}
</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{ asset('assets/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">

                            <p>
            {{\Illuminate\Support\Facades\Auth::guard('web')->user()->fname}}
                            </p>
                        </li>
                        <!-- Menu Body -->
                        
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-right">
                                <a href="{{url('/home/user/logout')}}" class="btn btn-default btn-flat">{{__('messages.logout')}}</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->

            </ul>
        </div>
    </nav>
</header>