<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Psr\Container\ContainerInterface;
//use Auth;
use App\Notification;

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//for user routes
Route::get('/', function () {
    return view('welcome');
});


//SocialLite
Route::get('login/{provider}', 'AuthSocController@redirectToProvider');
Route::get('login/{provider}/callback', 'AuthSocController@handleProviderCallback');


 





/// USER Routes

Route::get('home/user/logout', function () {
    Auth()->guard('web')->logout();
    return redirect('/login');
});




///////////////////////////////////////////

//Admin Routes
Route::get('/admin/login', 'admin@index');
Route::post('/admin/login', 'admin@loginpost')->name('admin.login.submit');

Route::get('/admin/forget/password','admin@forgetpassword');
Route::get('/admin/reset/password/{token}','admin@resetpassword');
Route::post('/admin/reset/password/{token}','admin@postresetpassword');
Route::post('/admin/forget/password','admin@postforgetpassword');


Config::set('auth.defines','admin');
Route::get('admin/logout', function () {
    Auth()->guard('admin')->logout();
    return redirect('/admin/login');
});


/// user route 
Route::group(['prefix' => LaravelLocalization::setLocale().'/home','middleware'=>'auth:web'],function(){

  
  


 Route::get('invoice/show/{search?}','userController\createinvoice@showinvoice');


 });






///admin route 
 Route::group(['prefix' => LaravelLocalization::setLocale().'/admin','middleware'=>'admin:admin'],function(){







     Route::get('','AdminController\AdminController@index')->name('admin.dashboard');



 });






/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//


Route::get('/ahmed',function (){



    dd(\Mcamara\LaravelLocalization\Facades\LaravelLocalization::getSupportedLocales());

});



Route::get('/order', function () {
    return view('front.order');
});

Route::get('/welcome/{locale}', function ($local) {
    \Illuminate\Support\Facades\App::setLocale($local);
    return view('welcome');
});





























//////////////////////////////////////////
/*
 *
 *

Route::group(['middleware'=>'news'],function(){


Route::get('insert', 'newscontroller@showdb');
Route::post('insertdb', 'newscontroller@insertdb');
});



Route::get('send/mail', function () {
//Mail::to('ayousry943@gmail.com')->send(new App\Mail\testmail());
//	\App\Jobs\sendmailjob::dispatch();

	$job = (new \App\Jobs\sendmailjob)->delay(\Carbon\Carbon::now()->addSeconds(1));
	dispatch($job);
	return  'mail sent';
});

 */

//Route::get('data/user', function () {
//
//if (Gate::allows('showdata',auth()->user())) {
// return  view('welcome');
//}else{
//  return  'you dont have pertmation  ';
//}
//});
